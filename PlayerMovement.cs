﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class PlayerMovement : MonoBehaviour
{
    public Animator animator;

    public GameObject crossHair;
    public GameObject arrowPrefab;

    public float coolDown = 1;
    public float coolDownTimer;

    void Update()
    {
        MoveCrossHair();

        Vector3 movement = new Vector3(XCI.GetAxis(XboxAxis.LeftStickX, XboxController.Any), XCI.GetAxis(XboxAxis.LeftStickY, XboxController.Any), 0.0f);

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Magnitude", movement.magnitude);

        transform.position = transform.position + movement * Time.deltaTime;
    }

    private void MoveCrossHair()
    {
        if(coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }


        Vector3 aim = new Vector3(XCI.GetAxis(XboxAxis.RightStickX, XboxController.Any), XCI.GetAxis(XboxAxis.RightStickY, XboxController.Any), 0.0f);
        Vector2 shootingDirection = new Vector2(XCI.GetAxis(XboxAxis.RightStickX, XboxController.Any), XCI.GetAxis(XboxAxis.RightStickY, XboxController.Any));

        if (aim.magnitude > 0.0f)
        {
            aim.Normalize();
            aim *= 0.4f;    
            crossHair.transform.localPosition = aim;
            crossHair.SetActive(true);

            shootingDirection.Normalize();
            if (XCI.GetAxis(XboxAxis.RightTrigger, XboxController.Any) > 0 && coolDownTimer == 0)
            {
                GameObject bullet = Instantiate(arrowPrefab, transform.position, Quaternion.identity);
                bullet.GetComponent<Rigidbody2D>().velocity = shootingDirection * 3.0f;
                bullet.transform.Rotate(0.0f, 0.0f, Mathf.Atan2(shootingDirection.y, shootingDirection.x) * Mathf.Deg2Rad);
                Destroy(bullet, 2.0f);

                coolDownTimer = coolDown;
            }
        }
        else
        {
            crossHair.SetActive(false);
        }
    }
}
