﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCrossHair : MonoBehaviour
{
    public float speed = 10f;
    public Transform Target;

    void Update()
    {
        FaceCrossHair();
    }

    void FaceCrossHair()
    {
        Vector2 direction = Target.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);
    }
}
